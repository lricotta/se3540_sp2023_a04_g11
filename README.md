# SE3540_SP2023_A04_G11

## Game Description
//game description

## How to Play
1. A Balut is a five-of-a-kind of any denomination, but unlike in the game Yahtzee it counts for little in terms of points.

2. Players who obtain a Balut announce it to the other players by calling out - Balut!” similar to calling out - bingo!

3. You must score 4 times in each category

4. If the total score in each category meets the requirement for that category (apart from one) you are awarded some points.

5. Once you have the dice face combination you want to score, you score the roll in one of the 7 categories.

6. Once a field has been scored, it is closed out for the rest of the game; you cannot change a field's score once it has been set.

7. In the first 3 categories you total only the specified die face.

8. So if you roll and score in the Sixes category, your total for that entry would be 18.

9. This same roll would yield zero if you scored it in the Fives and 4 if you scored it in the Fours category.

10. The fourth category is named Straights. Like in poker, a straight is a sequence of consecutive die faces.

11. A small straight is 5 consecutive faces, starting from 1. A large straight is 5 consecutive faces starting from 2 and ending at 6.

12. A small straight yields a score of 15 (1+2+3+4+5) and a large straight 20 (2+3+4+5+6).

13. Again as in poker, a Full House is a roll where you have both a 3 of a kind, and a pair.

14. Full house yield a score equal to the total number of eyes on all five die.

15. A Balut is a 5 of a Kind ( all the die faces are the same), and it yields a score of 20 plus all the die faces values.

## How to Win
1. The objective of the game is to have the most points.

2. The game ends when all 4 fields in all 7 categories have been scored.

## Rules
//write rules

## What Breaks the Game
//write game breaking bugs